##########################################################################

class Point:
    def __init__(self, x, y, z):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def to_string(self):
        return "%s %s %s" % (self.x, self.y, self.z)

    def get_ijk_values(self, Nx, Ny, Nz, dx, dy, dz):
        j = max(int(round(self.y / dy)), 0)
        j = min(j, Ny - 1)
        i = max(int(round(self.x / dx + (j % 2) / 2.)), 0)
        i = min(i, Nx - 1)
        k = max(int(round(self.z / dz - (i % 3) / 3. + 1 - (j % 2) / 3.)), 0)
        k = min(k, Nz - 1)
        return (i, j, k)

    @staticmethod
    def create_from_grid_coordinates(i, j, k, dx, dy, dz):
        x = (float(i) - (0.5) * (j % 2)) * dx
        y = (float(j)) * dy
        z = float(k) * dz + float(i % 3) * dz / 3. - dz + float(j % 2) * dz / \
            3.   # lowest z coordinate starts below xy plane.
        return Point(x, y, z)

    @staticmethod
    def midpoint(p1, p2):
        return Point(
            (p1.x + p2.x) / 2.,
            (p1.y + p2.y) / 2.,
            (p1.z + p2.z) / 2.)


class Triangle:
    def __init__(self, p1, p2, p3):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def to_string(self):
        return "%s\n%s\n%s\n" % (
            self.p1.to_string(), self.p2.to_string(), self.p3.to_string())
