# new_normal

![Dragon Before](/demo_files/dragon_before.png?raw=true "Dragon Before")

Above:  Input file dragon.off

Below: Output file nn_dragon.off



![Dragon After](/demo_files/dragon_after.png?raw=true "Dragon After")

## Installation
Use [pip](https://pip.pypa.io/en/stable/) to install dependencies.

```bash
pip3 install numpy
pip3 install trimesh
pip3 install scipy
pip3 install rtree
```

## Usage
To remesh a surface in file.off using approximately N^3 tetrahedra,
where N is an integer,
enter the command

```python
python3 newNormal.py N file.off
```

Python 3 or later is required.

This python program inputs a .off file and returns a new .off file
which has been processed using the gradNormal algorithm.

Calls on files geometric_objects.py, region_definition.py, resize_off_to_unit_cube.py

The output of this program is four .off files:

surfacePts.off   - output of MidNormal algorithm

gsurfacePts.off   - surfacePts.off projected to surface by closest point projection

noquadSurfacePts.off - adjusts gsurfacePts.off as in GradNormal algorithm by
replacing 4 triangles neighboring valence four vertices with 2 triangles.

nn_file.off
Surface rescaled to orginal rectangular box.

It also returns angle statistics.

To run the program, put these files into a folder with a .off file.
For an example, we use the file bun_noholes.off which contains the
Stanford bunny with its holes filled to make it a surface.

On a terminal command line enter
```python
python3 newNormal.py 100 bun_noholes.off
```
This example uses N=100.  Good meshes can be obtained with N as small as 5 for simple surfaces.  On a 2018 Macbook, the program runs in a few minutes with N up to 200.  Larger N lead to finer triangulations, with the diameter of a typical mesh triangle proportional to 1/N.

The program requires a "watertight" 2-dimensional manifold of any genus.  This means that it assumes no holes or 
edges meeting more than two triangles.  It will fail if the input .off file does not
represent a watertight surface.  For the armadillo.off file in the Examples folder, the Euler
characteristic is not 2, indicating a flaw in the mesh.  It will also fail on the standard
Stanford Bunny file, which has holes in the surface.  It works fine on horse.off and deer.off
which are manifold meshes.  The surface can be of any genus as long as it is watertight.


**Update, 9/12/21**

**Improved implementation**

Marc Bell had implemented a much improved version.

This is available at https://github.com/markcbell/midnormal.

