

X_TRANSLATION = 1
Y_TRANSLATION = 1
Z_TRANSLATION = 1

STRETCH_FACTOR = .5

with open("bun_noholes_cube.off", 'r') as in_f, open('translated_bun.off', 'w') as out_f:

    vertice_count = 0
    for index, line in enumerate(in_f):
        if index == 0:
            out_f.write(line)
        elif index == 1:
            count_info = line.split(' ')
            vertice_count = int(count_info[0])
            out_f.write(line)
        elif index < 2 + vertice_count:
            point = line.split(' ')
            translated_point = point

            translated_point[0] = str(
                (float(point[0]) + X_TRANSLATION) * STRETCH_FACTOR)
            translated_point[1] = str(
                (float(point[1]) + Y_TRANSLATION) * STRETCH_FACTOR)
            translated_point[2] = str(
                (float(point[2]) + Z_TRANSLATION) * STRETCH_FACTOR)
            out_f.write(" ".join(translated_point))
        else:
            out_f.write(line)
